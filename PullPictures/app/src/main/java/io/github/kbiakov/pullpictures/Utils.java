package io.github.kbiakov.pullpictures;

import org.apache.commons.validator.UrlValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class Utils {

    private static final String HTTP = "http";
    private static final String HTTPS = "https";

    private static final String COM = ".com";
    private static final String NET = ".net";
    private static final String RU = ".ru";

    public static boolean isValidUrl(String url) {
        String[] schemes = {HTTP, HTTPS};
        UrlValidator urlValidator = new UrlValidator(schemes);
        return urlValidator.isValid(url);
    }

    public static String getDataByUrl(String urlString) {
        String htmlData = "";
        try {
            URL url = new URL(urlString);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                htmlData += inputLine;
            }
        } catch (IOException ignored) {}

        return htmlData;
    }

    public static String getFullUrl(String base, String url) {
        if (url.contains(HTTP) || url.contains(HTTPS)) {
            return url;
        }

        if (url.contains(COM) || url.contains(NET) || url.contains(RU)) {
            return HTTP + base.substring(0, base.indexOf('/')) + url;
        }

        int thirdSlash = findSymEntry(base, '/', 3);
        return (thirdSlash != -1 ? base.substring(0, thirdSlash) : base) + url;
    }

    private static int findSymEntry(String s, char sym, int entry) {
        int w = 0, z = 0;
        while ((w = s.indexOf(sym, w)) >= 0) {
            if (++z > entry) break;
            w++;
        }
        return w;
    }

}
