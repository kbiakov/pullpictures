package io.github.kbiakov.pullpictures;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class PicturesActivity extends AppCompatActivity {

    private static final String EXTRA_URL = "url";

    private ProgressBar progressBar;
    private GridView gvPictures;

    private ArrayList<String> mPictureUrls = new ArrayList<>();
    private PicturesAdapter mPicturesAdapter;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);

        url = getIntent().getStringExtra(EXTRA_URL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) findViewById(R.id.toolbarTitle)).setText(url);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationIcon(R.drawable.ic_menu_back);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mPicturesAdapter = new PicturesAdapter(this, mPictureUrls, url);

        gvPictures = (GridView) findViewById(R.id.gvPictures);
        gvPictures.setAdapter(mPicturesAdapter);

        new GetAndPullTask().execute(url);
    }

    private class GetAndPullTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... urls) {
            return Utils.getDataByUrl(urls[0]);
        }

        @Override
        protected void onPostExecute(String data) {
            Document doc = Jsoup.parse(data);
            Elements picUrls = doc.select("img");

            for (int i = 0; i < picUrls.size(); i++) {
                mPictureUrls.add(picUrls.get(i).attr("src"));
            }

            mPicturesAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
