package io.github.kbiakov.pullpictures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class MainActivity extends AppCompatActivity {

    private static final String EXTRA_URL = "url";

    private EditText etUrl;
    private Button btnPull;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) findViewById(R.id.toolbarTitle)).setText(getString(R.string.app_name));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        initUIL();

        etUrl = (EditText) findViewById(R.id.etUrl);

        btnPull = (Button) findViewById(R.id.btnPull);
        btnPull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = etUrl.getText().toString();

                if (url.equals("")) return;

                if (Utils.isValidUrl(url)) {
                    Intent i = new Intent(MainActivity.this, PicturesActivity.class);
                    i.putExtra(EXTRA_URL, url);
                    startActivity(i);
                    return;
                }

                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.not_valid_url)
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
            }
        });
    }

    private void initUIL() {
        DisplayImageOptions options = new DisplayImageOptions
                .Builder()
                .cacheInMemory(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(getApplicationContext())
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);
    }

}
