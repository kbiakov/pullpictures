package io.github.kbiakov.pullpictures;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;


public class PicturesAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private ArrayList<String> mPicUrls;
    private String mUrl;

    private static class ViewHolder {
        private ImageView ivPicture;
    }

    public PicturesAdapter(Context context, ArrayList<String> picUrls, String url) {
        super(context, R.layout.item_picture, picUrls);
        this.mContext = context;
        this.mPicUrls = picUrls;
        this.mUrl = url;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_picture, null);

            vh = new ViewHolder();
            vh.ivPicture = (ImageView) convertView.findViewById(R.id.ivPicture);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.ivPicture.setImageResource(android.R.color.white);

        String picUrl = mPicUrls.get(position);

        if (picUrl != null) {
            String url = Utils.getFullUrl(mUrl, picUrl);
            Log.d("url", url);
            ImageLoader.getInstance().displayImage(url, vh.ivPicture);
        }

        return convertView;
    }

}
